function FormControl(props) {  
  return (
    <div className="form-control">
      <label htmlFor={props.inputId}>{props.labelText}</label>
      {
        props.type === "textarea"
        ?
        <textarea placeholder={props.value} id={props.inputId} cols="30" rows="10">
          moto
        </textarea>
        :
        <input placeholder={props.value} id={props.inputId} type={props.type}/>
      }
    </div>
  )

}

export default FormControl