import React, {Component} from "react";

class Message extends Component {

  constructor () {
    super()
    this.state = {
      message : "Welcome User",
      test: "123123"
    }
  }

  changeState() {
    this.setState({
      message : 'Thanks for subscribing',
      test: "Fsdgdgsdgs"
    })  
  }

  render() {
    return (
      <div>
        <h2>{this.state.message}</h2>
        <h2>{this.state.test}</h2>
        <button onClick={() => this.changeState()}>Subscribe</button>
      </div>
    )
  }
}



export default Message;