import { Component } from 'react';
const USER_DATA = [
    {
        username: 'ahousego0',
        email: 'sandreini0@mozilla.org',
    },
    {
        username: 'esevier1',
        email: 'wmaddrell1@fda.gov',
    },
    {
        username: 'jyounie2',
        email: 'nmontes2@sohu.com',
    },
    {
        username: 'bsanday3',
        email: 'sgarlic3@goodreads.com',
    },
    {
        username: 'ahaughan4',
        email: 'tmcinally4@hatena.ne.jp',
    },
    {
        username: 'ecornil5',
        email: 'fjuschke5@163.com',
    },
    {
        username: 'egodilington6',
        email: 'wbradder6@weebly.com',
    },
    {
        username: 'dmccomas7',
        email: 'obasketter7@foxnews.com',
    },
    {
        username: 'pgosenell8',
        email: 'hrapi8@google.com.hk',
    },
    {
        username: 'abreakspear9',
        email: 'dcourse9@ibm.com',
    },
    {
        username: 'mrichtera',
        email: 'zgerrya@nih.gov',
    },
    {
        username: 'mbrachellb',
        email: 'ktinmanb@wunderground.com',
    },
    {
        username: 'apercivalc',
        email: 'kbampforthc@elpais.com',
    },
    {
        username: 'nstorahd',
        email: 'cdevonportd@rakuten.co.jp',
    },
];
class UserList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: USER_DATA,
            column: '',
            direction: 'asc',
			filter: '',
			filteredUsers: USER_DATA,
        }
    }

    sortByUsername = () => {
        let sortedArray = this.state.users.slice();
        let direction = this.state.direction;

        if(this.state.column === 'username') {
            direction = direction === 'asc' ? 'desc' : 'asc';
        }

        sortedArray.sort((a,b) => {
            if(direction === 'asc') {
                //asc sort
                return a.username > b.username ? 1 : a.username < b.username ? -1 : 0;
            } else {
                //desc sort
                return a.username > b.username ? -1 : a.username < b.username ? 1 : 0;
            }
        });

        this.setState({
            users: sortedArray,
            column: 'username',
            direction: direction,
        });
    }

    sortByEmail = () => {
        let sortedArray = this.state.users.slice();
        let direction = this.state.direction;

        if(this.state.column === 'email') {
            direction = direction === 'asc' ? 'desc' : 'asc';
        }

        sortedArray.sort((a,b) => {
            if(direction === 'asc') {
                return a.email > b.email ? 1 : a.email < b.email ? -1 : 0;
            } else {
                return a.email > b.email ? -1 : a.email < b.email ? 1 : 0;
            }
        });

        this.setState({
            users: sortedArray,
            column: 'email',
            direction: direction,
        })
    }

	inputFilter = (e) => {
		let val = e.target.value;
		console.log(val)

		this.setState({
			filter: val,
		}, () => {
			this.getFilterUsers()
		})
	}

	getFilterUsers = () => {
		let filterUsers = this.state.users;
		let result = filterUsers.filter(user => user.username.includes(this.state.filter) || user.email.includes(this.state.filter))
		
		if(this.state.filter === '') {
			result = this.state.users
		}

		this.setState({
			filteredUsers: result,
		})
	}

    render() {
        return (
            <>
				<div className="form-control">
					<label htmlFor="filter">Filter</label>
					<input className="filter-input" name="filter" id="filter" type="text" placeholder="Search User" onChange={this.inputFilter }/>
				</div>
                <table className="table table-dark">
                    <thead>
                        <tr className="table-dark">
                            <th>#</th>
                            <th>
                                <button className="btn btn-info" onClick={this.sortByUsername}>
                                    <span>Username</span>
                                    {this.state.column === 'username' &&
                                        <span className={[
                                            "fas",
                                            this.state.direction === 'asc' ? "fa-angle-up" : "fa-angle-down"
                                        ].join(' ')}></span>
                                    }
                                </button>
                            </th>
                            <th>
                                <button className="btn btn-info" onClick={this.sortByEmail}>
                                    <span>Email</span>
                                    {this.state.column === 'email' && 
                                        <span className={[
                                            "fas",
                                            this.state.direction === 'asc' ? "fa-angle-up" : "fa-angle-down"
                                        ].join(' ')}></span>
                                    }
                                </button>
                            </th>
                        </tr>
                    </thead>
                    <tbody>                    
                        {this.state.filteredUsers.map((user, index) => {
                            return(
                                <tr key={index}>
                                    <td>{index+1}</td>
                                    <td>{user.username}</td>
                                    <td>{user.email}</td>
								</tr>
                            );
                        })}
                    </tbody>
					<tfoot>
						<tr>
							<th></th>
							<th colSpan="3">There {this.state.filteredUsers.length === 1 ? 'is' : 'are'} {this.state.filteredUsers.length} {this.state.filteredUsers.length === 1 ? 'result' : 'results'}</th>
						</tr>
					</tfoot>
                </table>
            </>
        );
    }
}
export default UserList;