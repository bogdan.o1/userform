import React, {Component} from "react";

class Counter extends Component {

  constructor () {
    super()
    this.state = {
      count : 0,
    }
  }

  incrementCounter = () => {
    this.setState( {count : this.state.count + 1}, () => { console.log(this.state.count) } )
  }

  renderOrange = () => {
    let portocale = []
    for(let i=0;i < this.state.count; i++) {
      portocale.push('🍊')
    }
    return portocale
  }

  render() {
    return (
      <div>
        <h1>Counter - {this.state.count}</h1>
        <button onClick={this.incrementCounter}>Increment Counter</button>
        {this.renderOrange()}
      </div>
    )
  }
}

export default Counter;